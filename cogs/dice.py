import random
from discord.ext import commands


roll_desc = 'Roll your selection of dice. \n \n For example:\n    !roll 2d10 to roll two ten sided dice, or !roll 2d10+4 to roll two ten sided dice with a +4 modifier. \n \n    !roll <1-20>d<4|6|8|10|12|20|100>(<+|-><1-100>'
class Dice:
    def __init__(self, dicebot):
        self.dicebot = dicebot
    
    @commands.command(pass_context=True, description=roll_desc)
    async def roll(self, ctx, *roll_string):
        roller_name = ctx.message.author.name
        format_string = 'Please use the format `!roll <1-20>d<4|6|8|10|12|20|100>(<+|-><1-100>).'
        roll_string = ''.join(roll_string)
        try:
            if roll_string:
                if "+" in roll_string:
                    split_roll_string = roll_string.lower().split('+')
                    mod="pos"
                if "-" in roll_string:
                    split_roll_string = roll_string.lower().split('-')
                    mod="neg"
                if "+" in roll_string or "-" in roll_string:
                    if int(split_roll_string[1]) <= 100 and int(split_roll_string[1]) > 0:
                        roll_mod = int(split_roll_string[1])
                        new_roll_string = split_roll_string[0]
                    else:
                        self.dicebot.say(format_string)
                else:
                    new_roll_string = roll_string

                dice = new_roll_string.split('d')
                num_dice = int(dice[0])
                die_size = int (dice[1])


                if die_size in [4, 6, 8, 10, 12, 20, 100] and num_dice > 0 and num_dice <= 20:
                    rolls = []
                    for roll in range(0, int(num_dice)):
                        rolls.append(random.randrange(1, int(die_size) + 1))
                    
                    if 'roll_mod' in locals():
                        if mod == 'pos':
                            total_roll = sum(rolls) + roll_mod
                        if mod == 'neg':
                            total_roll = sum(rolls) - roll_mod
                    else:
                        total_roll = sum(rolls)

                    rolls = ', '.join(str(result) for result in rolls)
                    if 'roll_mod' in locals():
                        if mod == 'pos':
                            return await self.dicebot.say('**{}** rolled {}. ({}) + {}'.format(roller_name, total_roll, rolls, roll_mod))
                        if mod == 'neg':
                            return await self.dicebot.say('**{}** rolled {}. ({}) - {}'.format(roller_name, total_roll, rolls, roll_mod))
                    else:
                        return await self.dicebot.say('**{}** rolled {}. ({})'.format(roller_name, total_roll, rolls))
                else:
                    return await self.dicebot.say(format_string)
            else:
                return await self.dicebot.say(format_string)
        except ValueError:
            return await self.dicebot.say(format_string)

        
def setup(dicebot):
    dicebot.add_cog(Dice(dicebot))