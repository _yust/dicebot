import discord
import secrets
from discord.ext.commands import Bot

dicebot = Bot(command_prefix='!')
startup_extensions = ['cogs.dice'] 

@dicebot.event
async def on_ready():
    print('Client logged in.')
    print(dicebot.user.name)
    print(dicebot.user.id)
    print('-----')

    await dicebot.change_presence(game=discord.Game(name="Dice Rollin"))

    for extension in startup_extensions:
        try:
            dicebot.load_extension(extension)
        except Exception as e:
            exc = '{}: {}'.format(type(e).__name__, e)
            print('Failed to load extensions {}\n {}'.format(extension, exc))


dicebot.run(secrets.BOT_TOKEN)